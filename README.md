# Myapp

### Práctica | Mi primera aplicación nativa

Docente: Luis Antonio Ramírez Martínez

Alumna: Jessica Melissa Núñez Castro

Matricula: 348509


## Instrucciones

Aplicando los conocimientos básicos obtenidos en clase vamos a desarrollar una aplicación que nos muestre el clima en nuestro celular, la cual deberá permitirnos implementar los fundamentos básicos de la navegación simple, el consumo de web services y el mapeo de estos a objetos dentro de nuestra aplicación.

Envíe la URL de gitlab de su proyecto como entregable de esta actividad
